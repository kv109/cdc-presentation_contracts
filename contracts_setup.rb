RSpec.configure do |config|
  config.before(:each) do
    api_host = Regexp.new('http://contracts.org')
    stub_request(:any, api_host).to_rack(Contracts)
  end
end
